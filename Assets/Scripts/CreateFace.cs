using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class CreateFace : MonoBehaviour
{
    [Header("Scene Objects")]
    public Transform faceParent;
    [Header("Prefabs")]
    public List<Transform> heads;
    public List<Transform> eyeballs;
    Transform head;
    Transform eyeball;
    public Vector3 headScale;

    void Start()
    {
        CreateHead();
    }

    void CreateHead()
    {
        head = Instantiate(heads.RandomInRange());
        head.SetParent(faceParent);
        head.name = "HeadBase";
        headScale = new Vector3(
            Random.Range(0.5f, 1.5f),
            Random.Range(0.5f, 1.5f),
            Random.Range(0.5f, 1.5f));
//        head.localScale = headScale;
        CreateEye();
    }

    void CreateEye()
    {
        var randomPosition = Random.onUnitSphere;
        RaycastHit hit;
        if(Physics.Raycast(head.position + randomPosition, -randomPosition, out hit)) {
            Debug.DrawRay(head.position + randomPosition, -randomPosition,Color.green,5);
            eyeball = Instantiate(eyeballs.RandomInRange());
            eyeball.position = hit.point;
            eyeball.rotation = Quaternion.FromToRotation(Vector3.zero, randomPosition);
            eyeball.SetParent(head);
        };

    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) { ResetHead();}
//        Debug.DrawRay(head.position,eyeAngle, Color.red);
    }

    void ResetHead()
    {
        Destroy(head.gameObject);
        CreateHead();
    }
}
